"""
interpreter_script.py

This script demonstrates the use of StringInterpreter and TernaryInterpreter classes to
interpret user input and print the results.

Usage:
    $ python interpreter_script.py

Author:
    [Your Name]

Date:
    [Current Date]
"""

from string_to_dict_interpreter import StringInterpreter as Si
from ternary_operator_practice import TernaryInterpreter as Ti


def run_interpreter(interpreter: Si | Ti) -> None:
    """
    Runs the specified interpreter and prints the result.

    Parameters:
        interpreter: An instance of the interpreter class.

    Returns:
        None
    """
    print(interpreter.get_result())


if __name__ == "__main__":
    # 1
    # Prompt the user to enter a string
    user_string = input("Enter string:")
    run_interpreter(Si(user_string))

    # 2

    cases = (
        (1, 2, "3"),
        (1, 1, "0"),
        (2, 1, "1"),
        (0, 0, "game over"),
        (1, 1, "game over"),  # It's wrong to check
    )

    for x, y, result in cases:
        ti = Ti(x, y)
        func_res = ti.get_result()
        assert (
            func_res == result
        ), f"ERROR: for x = {x} and y = {y} returned {func_res}, but expected: {result}"
